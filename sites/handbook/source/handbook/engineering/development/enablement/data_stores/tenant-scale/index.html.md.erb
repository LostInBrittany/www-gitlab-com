---
layout: handbook-page-toc
title: Tenant Scale Group
description: "The Tenant Scale Group is the direct outcome of applying our value of Iteration to the direction of the Database Scalability Working Group."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## About

The Tenant Scale group (formerly Pods or Sharding group) is part of the [Data
Stores stage](/handbook/engineering/development/enablement/data_stores/). We
offer support for groups, projects, and user profiles within our product, but
our main focus is a long-term horizontal scaling solution for GitLab.

This page covers processes and information specific to the Tenant Scale group.
See also the [direction page](/direction/enablement/tenant-scale/) and the
[features we support per category](/handbook/product/categories/features/#data-storestenant-scale-group).

### Contact

To get in touch with us, it's best to create an issue in the relevant
project (typically [GitLab](https://gitlab.com/gitlab-org/gitlab)) and add the
`~"group::tenant scale"` label, along with any other appropriate labels.

For urgent items, feel free to use the Slack channel (internal): [#g_tenant-scale](https://gitlab.slack.com/archives/g_tenant-scale).

### Vision

There are multiple proposals and ideas to increase horizontal scalability via
solutions such as database sharding and tenant isolation. The objective of this
group is to explore, iterate on, validate, and lead implementation of proposals
to provide a solution to accommodate GitLab.com's daily-active user growth.

As we brainstorm and iterate on horizontal scalability proposals, we will
provide implementation details, prototypes, metrics, demos, and documentation to
support our hypotheses and outcomes.

Currently, [Cells](https://docs.gitlab.com/ee/architecture/blueprints/cells/) is
our proposal of a new architecture for our SaaS that is horizontally scalable,
resilient, and provides a more consistent user experience.

### Goals

The executive summary goals for the Tenant Scale group include:

- Support GitLab.com's daily-active user growth
- Do not allow a problem with any given data store to affect all users
- Minimize or eliminate complexity for our self-managed use-case

### Team Members

The following people are permanent members of the Tenant Scale group:

<%= direct_team(manager_slug: 'arturo-herrero') %>

### Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(
  role_regexp: /(Tenant Scale|Principal Engineer, Data Stores|Distinguished Engineer, Ops and Enablement)/,
  direct_manager_role: 'Engineering Manager, Tenant Scale'
) %>

### Team IC Gearing

**Exception Ratio**: 1 Distinguished Engineer, multiple Staff Engineers.

**Justification**: The Tenant Scale group was formed to implement a sharding
solution to solve our database limitations as outlined by the [Database
Scalability Working Group](https://about.gitlab.com/company/team/structure/working-groups/database-scalability/).
The project requires deep knowledge of both PostgreSQL and the application
itself. As the working group has noted, _this problem cannot be solved to meet
our needs and requirements if we limit ourselves to the database: we must
consider careful changes in the application to make it a reality_. Given the
complexity of the task, we have carefully crafted a team with a mix of interests
and expertise to achieve success. We also anticipate the team will continue to
require the expertise of multiple staff+ level engineers to accomplish our
horizontal scalability goals.

## Meetings

We are a globally distributed group and we communicate mostly asynchronously,
however, we also have synchronous meetings. It's unlikely everyone can attend
those meetings, so we record them and share written summaries ([agenda](https://docs.google.com/document/d/1W7QsQL_2wMLW9KJU5ZEZdyIqYtRC6_bwOoNXJUWNbiU/edit)).
Currently we have the following recurring meetings scheduled:

 - Weekly Monday - Tenant Scale Group Sync (APAC/EMEA) 8:30AM UTC (2:30AM PDT)
 - Weekly Thursday - Tenant Scale Group Sync (EMEA/AMER) 2:30PM UTC (7:30AM PDT)

## Work

The Product Manager compiles the list of issues following
the [product prioritization process](/handbook/product/product-processes/#prioritization),
with input from the team, Engineering Manager, and other stakeholders.
The iteration cycle lasts from the 18th of one month until the 17th of the next month,
and is identified by the GitLab version set to be released on the 22nd.

Engineers are encouraged to work as closely as needed with their stable
counterparts. Quality engineering is included in our workflow via the
[quad planning process](/handbook/engineering/quality/quality-engineering/quad-planning/).

### Milestone Planning

Before starting a milestone, the group coordinates using [planning issues](https://gitlab.com/gitlab-org/tenant-scale-group/group-tasks/-/issues/?label_name%5B%5D=Planning%20Issue).
We follow this process:
- The Product Manager defines the goals of the milestone.
- The team members comment about the issues they consider relevant for the milestone.
- The Product Manager and Engineering Manager work together to decide the final list of issues.
- The whole team reviews the items lined up before the milestone begins.

### What To Work On

The primary source for things to work on is the [milestone prioritization board](https://gitlab.com/groups/gitlab-org/-/boards/5548886),
which lists all issues scheduled for the current cycle in priority order (from most to least important): p1,
p2, and p3. You should first pick up issues that have the highest priority, which are listed at the top of the first board column.
When you assign yourself to an issue, you indicate that you are working on it.

If anything is blocking you from getting started with the top issue immediately,
like unanswered questions or unclear requirements, you can skip it, as long as
you put your findings and questions in the issue.
This helps the next engineer who picks up the issue.

Usually issues are not directly assigned to people, except when
a person has clearly the most knowledge or context to work on an issue.

### Development Workflow

We follow the GitLab [engineering workflow](/handbook/engineering/workflow/)
guidelines. To get a high-level overview of the status of all issues in the
current milestone, check the [development workflow board](https://gitlab.com/groups/gitlab-org/-/boards/2594854).

As owners of the issues assigned to them, engineers are expected to keep the
workflow labels on their issues up to date. When an engineer starts working an
issue, they mark it with the `workflow::in dev` label as the starting point
and continue [updating the issue throughout development](/handbook/engineering/workflow/#updating-issues-throughout-development).
Before closing an issue, it's important to add the `workflow::complete` label, because this is one
of the requirements for the completed items to appear in the Improvements and Bugs
overview of each month's release post. The process primarily follows this diagram:

``` mermaid
graph LR

  classDef workflowLabel fill:#428BCA,color:#fff;

  A(workflow::in dev):::workflowLabel
  B(workflow::in review):::workflowLabel
  C(workflow::verification):::workflowLabel
  F(workflow::complete):::workflowLabel

  A -- Push an MR --> B
  B -- Merged --> C
  C --> D{Works on production?}
  D -- YES --> F
  F --> CLOSE
  D -- NO --> E[New MR]
  E --> A
```

### Issue Boards

We track our work on the following issue boards:

- [Data Stores:Tenant Scale milestone prioritization](https://gitlab.com/groups/gitlab-org/-/boards/5548886)
- [Data Stores:Tenant Scale cross-functional prioritization](https://gitlab.com/groups/gitlab-org/-/boards/4424394)
- [Data Stores:Tenant Scale planning](https://gitlab.com/gitlab-org/gitlab/-/boards/3782673)
- [Data Stores:Tenant Scale validation](https://gitlab.com/gitlab-org/gitlab/-/boards/4403984)
- [Data Stores:Tenant Scale build](https://gitlab.com/groups/gitlab-org/-/boards/2594854)
- [Data Stores:Tenant Scale bug](https://gitlab.com/gitlab-org/gitlab/-/boards/4620745)
- [Data Stores:Tenant Scale release posts](https://gitlab.com/gitlab-org/gitlab/-/boards/4908764)
- [Data Stores:Tenant Scale milestones](https://gitlab.com/groups/gitlab-org/-/boards/5549104)
- [Data Stores:Tenant Scale team members](https://gitlab.com/groups/gitlab-org/-/boards/5549106)
- [Data Stores:Tenant Scale important](https://gitlab.com/groups/gitlab-org/-/boards/1438588)

## Dashboards

<%= partial "handbook/engineering/metrics/partials/_cross_functional_dashboard.erb", locals: { filter_value: "Tenant Scale" } %>
